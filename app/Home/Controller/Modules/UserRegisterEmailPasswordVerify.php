<?php
/*
 * 根据邮箱，密码，验证码注册
 * 用于在 /User/register 页面
 *
 * 需要预先设定的值
 *      $title [可选]
 *
 */
if (!defined('THINK_PATH')) {
    exit();
}
if (IS_POST) {
    $verify = new Think\Verify ();
    if (!$verify->check(I('post.verify'), 'user_register')) {
        $this->error('验证码错误');
    }
    $email = I('post.email', '', 'trim');
    $password = I('post.password');
    $password_repeat = I('post.password-repeat');

    if ($password != $password_repeat) {
        $this->error('两次密码输入不一致');
    }

    $msg = $this->_am->register($email, $cellphone = '', $username = '', $password, $add_data = array(), $ret_uid);
    if (true === $msg) {

        if (has_module('MemberUpload')) {
            $this->_am->init($ret_uid);
            $this->_am->update('upload_space', array(
                'space' => tpx_config_get('member_upload_space')
            ));
        }

        $this->success('', U('User/login'));
    } else {
        $this->error($msg);
    }
}
if (empty($title)) {
    $title = tpx_config_get('home_title', '');
}
$this->assign('page_title', '新用户注册 - ' . $title);
$this->assign('page_keywords', '新用户注册,' . $title . '注册');
$this->assign('page_description', '注册成为' . $title . '新用户');
$this->display();