<?php
/*
 * 用户注册验证码
 * 用户 /User/forgetpwd_verify
 *
 */
if (!defined('THINK_PATH')) {
    exit();
}
$verify = new \Think\Verify ();
$verify->length = 4;
$verify->entry('user_forgetpwd');