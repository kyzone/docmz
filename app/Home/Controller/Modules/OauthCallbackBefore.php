<?php
/*
 * 根据邮箱，密码，验证码注册
 * 用于在 /Oauth/Callback 页面
 *
 * 需要预先设定的值
 *      $title [可选]
 *      $redirect [可选]
 */
if (!defined('THINK_PATH')) {
    exit();
}

$token = null;
$openid = null;

if (! in_array ( $type, array (
        'qq',
        'sina' 
) )) {
    $this->error ( 'Error Request [type=' . $type . ',code=' . $code . ']!' );
}

$sns = \ThinkOauth::getInstance ( $type );
$extend = null;
$token = $sns->getAccessToken ( $code, $extend );
$openid = $sns->openid ();

if (! $token || ! $openid) {
    $this->error ( 'Error Request [token=' . $token . ',openid=' . $openid . ']!' );
}