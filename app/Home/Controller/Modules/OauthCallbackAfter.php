<?php
/*
 * 根据邮箱，密码，验证码注册
 * 用于在 /Oauth/Callback 页面
 *
 * 需要预先设定的值
 *      $title [可选]
 *      $redirect [可选]
 */
if (!defined('THINK_PATH')) {
    exit();
}

$_SESSION ['member_user_uid'] = $uid;
$redirect = I('session.login_redirect',U('/'));
if(isset($_SESSION['login_redirect'])){
    unset($_SESSION['login_redirect']);
}
header ( 'Location: '.$redirect );