<?php
/*
 * 根据邮箱，密码，验证码注册
 * 用于在 /Oauth/jump 页面
 *
 * 需要预先设定的值
 *      $title [可选]
 *      $redirect [可选]
 */
if (!defined('THINK_PATH')) {
    exit();
}

if (!in_array($type, array(
    'qq',
    'sina'
))
) {
    $this->error('Error Request [type=' . $type . ']!');
}
$sns = \ThinkOauth::getInstance($type);
$url = $sns->getRequestCodeURL();

if (empty($redirect) && empty($_SESSION['login_redirect'])) {
    $redirect = I('server.HTTP_REFERER');
}

if (!empty($redirect)) {
    $_SESSION['login_redirect'] = $redirect;
}

header("Location: $url");