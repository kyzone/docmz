<?php
/*
 * 用户注册验证码
 * 用户 /User/register_verify页面
 *
 */
if (!defined('THINK_PATH')) {
    exit();
}
$verify = new \Think\Verify ();
$verify->length = 4;
$verify->entry('user_register');