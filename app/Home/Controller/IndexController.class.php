<?php

namespace Home\Controller;


class IndexController extends BaseController
{
    public function index()
    {
        $this->assign('page_title', tpx_config_get('home_title'));
        $this->assign('page_keywords', tpx_config_get('home_keywords'));
        $this->assign('page_description', tpx_config_get('home_description'));
        $this->display();
    }

}