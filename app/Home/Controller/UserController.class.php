<?php

namespace Home\Controller;


class UserController extends BaseController
{
    public function index()
    {
        $this->display();
    }

    public function login()
    {
        include './app/Home/Controller/Modules/UserLoginUsernamePassword.php';
    }

    public function register()
    {
        include './app/Home/Controller/Modules/UserRegisterUsernamePassword.php';
    }

    public function logout()
    {
        unset ($_SESSION ['member_user_uid']);
    }

    public function changepwd()
    {
        include './app/Home/Controller/Modules/UserChangepwd.php';
    }

    public function profile()
    {
        $this->display();
    }

    public function load_profile()
    {
        $data = array();

        $data['username'] = $this->_am->get('user.username');
        $data['upload_space_space'] = byte_format(intval($this->_am->get('upload_space.space')));

        $one = D('MemberUpload')->table('__MEMBER_UPLOAD__ mu')->join('__DATA_FILES__ df ON df.id=mu.data_id')
            ->field('sum(df.filesize) as total')->where(array('mu.uid' => MEMBER_LOGINED_UID))->find();
        $upload_size_current = $one['total'];

        $data['upload_space_used'] = byte_format($upload_size_current);

        $this->ajaxReturn(array(
            'status' => 1,
            'data' => $data
        ));
    }
}