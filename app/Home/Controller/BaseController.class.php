<?php

namespace Home\Controller;

use Think\Controller;

class BaseController extends Controller
{

    protected function _initialize()
    {
        if (file_exists($file = './app/Common/Custom/app_env_config.php')) {
            include $file;
        }

        if (!file_exists('./_CFG/install.lock')) {
            header('Location: ' . __ROOT__ . '/admin.php');
        }

        $domains = C('APP_SUB_DOMAIN_RULES');
        if (empty($domains)) {
            $this->error('您没有绑定任何域名或IP');
        }
        if (!isset($domains[HTTP_HOST])) {
            $this->error('您访问的域名' . HTTP_HOST . '没有绑定到该系统');
        }


        // 登陆用户检查
        if (file_exists($file = './app/Home/Controller/Modules/MemberInit.php')) {
            include $file;
        }

        // 未登录可以访问的页面

        $url = strtolower(CONTROLLER_NAME . '/' . ACTION_NAME);
        if (MEMBER_LOGINED) {
            $urlForbiddenWithLogin = array('user/login', 'user/register');
            if (in_array($url, $urlForbiddenWithLogin)) {
                $this->ajaxReturn(array(
                    'status' => 0,
                    'info' => 'login_success',
                    'username' => $this->data_username
                ));
            }
        } else {
            $urlPermitWithoutLogin = array('user/login', 'user/register', 'index/index');
            if (!in_array($url, $urlPermitWithoutLogin)) {
                $this->ajaxReturn(array(
                    'status' => 0,
                    'info' => 'login_required'
                ));
            }
        }

    }
}