<?php

namespace Admin\Controller;

class CmsDocDataController extends CmsController
{
    // 导出菜单
    // cmslist、cmshandle为必须要到导出的字段
    static $export_menu = array(
        'content' => array(
            '用户文档' => array(
                'cmslist' => array(
                    'title' => '文档正文',
                    'hiddens' => array(
                        'cmshandle' => '文档正文管理'
                    )
                )
            )
        )
    );
    // 标识字段，该字段为自增长字段
    public $cms_pk = 'id';
    // 数据表名称
    public $cms_table = 'cms_doc_data';
    // 数据库引擎
    public $cms_db_engine = 'MyISAM';
    // 列表列出的列出字段
    public $cms_fields_list = array(
        'id',
        'doc_id'
    );
    // 添加字段，留空表示所有字节均为添加项
    public $cms_fields_add = array();
    // 编辑字段，留空表示所有字节均为编辑项
    public $cms_fields_edit = array();
    // 搜索字段，表示列表搜索字段
    public $cms_fields_search = array();
    // 数据表字段
    public $cms_fields = array(

        'doc_id' => array(
            'title' => '文档ID',
            'description' => '',
            'type' => 'cms_id',
            'data' => 'cms_doc|id|title',
            'default' => '0',
            'rules' => 'searchable|required|unsigned|readonly'
        ),

        'content' => array(
            'title' => 'Markdown内容',
            'description' => '',
            'type' => 'bigtext',
            'default' => '',
            'rules' => ''
        ),

    );
}
