# Doc.MZ文档管理系统

## 简介

- 多人Markdown文档管理系统；
- 运行环境:PHP5.3+, MySQL5.0；
- 方便小团体内部使用的Markdown文档管理系统。

## 系统演示

- 预览：[http://doc.tecmz.com](http://doc.tecmz.com)
- 示例文档：[http://doc.tecmz.com/r/gamektTvFFQxdyFrGrHdwSQEHCaTVxiFNTDlyntHCWPunSfFUh:preview](http://doc.tecmz.com/r/gamektTvFFQxdyFrGrHdwSQEHCaTVxiFNTDlyntHCWPunSfFUh:preview)


## 系统特性

### 完全Markdown

完全支持Markdown基本语法和扩展语法（流程图、时序图、函数公式）

### 人性化显示方式

- **URL访问：**唯一的网络访问地址；
- **HTML下载：**生成单独HTML页面，所有资源完全静态化；
- **PDF打印：**生成方便打印的PDF页面。

### 历史版本保存

保存最近更新的50个历史版本，方便误操作恢复；


## 伪静态设置

### Apache环境.htaccess文件

	RewriteEngine On
	RewriteBase /
	RewriteRule ^/?$ - [NC,L]
	RewriteRule ^(app/|data/|asserts/|res/|_RUN/|robots\.txt|crossdomain\.xml).*$ - [NC,L]
	RewriteRule ^([a-z0-9]+)\.php - [NC,L]
	RewriteRule ^(.*)$ index.php/$1  [NC,L]

### Nginx环境*.conf文件
	location / {
    	index index.php;
    	if ( !-e $request_filename ) {
            rewrite ^(.*)$ /index.php?s=$1 last;
            break;
        }
    }

    location ~ \.php$ {
        fastcgi_pass   127.0.0.1:9000;
        fastcgi_index  index.php;
        fastcgi_param  PHP_VALUE  "open_basedir=$document_root:/tmp/";
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~* ^/(app|data|asserts|robots\.txt|crossdomain\.xml)/.*$ {
        if ( -f $request_filename ) {
            expires max;
            break;
        }
    }