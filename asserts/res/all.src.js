'use strict';
require.config({
    paths: {
        'app': '../res/app',
        'controller': '../res/controller',
        'router': '../res/router',
        'service': '../res/service'
    }
});

var markdownEditor = null;

require(['angular', 'app', 'controller', 'router', 'service', 'jquery.extern', 'bootstrap', 'lhgdialog.lang', 'lhgdialog.base'], function (angular, app) {
    angular.bootstrap($('#ng-app-lazy').get(0), ['app']);

    $(document).on('mouseover', '[data-toggle="tooltip"]', function () {
        $(this).tooltip('show');
    });

});
