'use strict';
define('controller', ['app'], function (app) {

    var loading = {
        cnt: 0,
        on: function () {
            var o = $('#status-loading');
            loading.cnt++;
            o.css({
                left: ($(window).width() - $(o).width()) / 2 + 'px'
            }).css({
                visibility: 'visible'
            });
        },
        off: function () {
            loading.cnt--;
            if (loading.cnt <= 0) {
                $('#status-loading').css({
                    visibility: 'hidden'
                });
            }
        },
        success_timeout: null,
        success: function (text, timeout) {
            var o = $('#status-success');
            timeout = timeout || 1500;
            o.html(text).css({
                left: ($(window).width() - $(o).width()) / 2 + 'px'
            }).css({
                visibility: 'visible'
            });
            if (loading.success_timeout) {
                clearTimeout(loading.success_timeout);
            }
            loading.success_timeout = setTimeout(function () {
                o.css({
                    visibility: 'hidden'
                });
            }, timeout);
        },
        error_timeout: null,
        error: function (text, timeout) {
            var o = $('#status-error');
            timeout = timeout || 1500;
            o.html(text).css({
                left: ($(window).width() - $(o).width()) / 2 + 'px'
            }).css({
                visibility: 'visible'
            });
            if (loading.error_timeout) {
                clearTimeout(loading.error_timeout);
            }
            loading.error_timeout = setTimeout(function () {
                o.css({
                    visibility: 'hidden'
                });
            }, timeout);
        }
    };

    app.controller("IndexIndexController", ['$scope', '$rootScope', '$state', 'UserService', function ($scope, $rootScope, $state, userService) {

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            //console.log(fromState);
            //console.log(toState);
        });

        //$rootScope.$on('$stateChangeStart', function (scope, next, current) {
        //    console.log('current', current);
        //    console.log('next', next);
        //    if ((typeof next == 'object') && ('$$route' in next)) {
        //        if (userService.user.logined) {
        //            var controllerForbiddenWithLogin = ['UserLoginController', 'UserRegisterController'];
        //            if (controllerForbiddenWithLogin.indexOf() >= 0) {
        //                console.log('已登录，跳转到 /');
        //                $state.go('user');
        //            }
        //        } else {
        //            var controllerPermitWithoutLogin = ['UserLoginController', 'UserRegisterController', 'IndexIndexController'];
        //            if (controllerPermitWithoutLogin.indexOf(next.$$route.controller) == -1) {
        //                console.log('未登录，跳转到 /login');
        //                $state.go('login');
        //            }
        //        }
        //    }
        //});
    }]);

    app.controller('IndexHeaderController', ['$scope', '$state', '$http', 'UserService', function ($scope, $state, $http, userService) {
        $scope.data = {
            username: userService.user.username
        };
        $scope.$state = $state;
        $scope.logout = function () {
            $http.get($.urlBuild('user', 'logout'), {cache: false}).success(function (data) {
                userService.user.logined = false;
                userService.user.username = '';
                window.location.href = TPX.PATH_ROOT + '/';
            });
        };
    }]);


    app.controller('UserIndexController', ['$scope', '$state', '$http', '$stateParams', 'UserService', function ($scope, $state, $http, $stateParams, userService) {
        $scope.data = {
            username: userService.user.username
        };
        if ('user' == $state.current.name) {
            $state.go('user.profile');
        }
        $scope.$state = $state;

    }]);

    app.controller('UserRegisterController', ['$scope', '$state', 'UserService', function ($scope, $state, userService) {
        $scope.data = {
            username: '',
            password: '',
            'password-repeat': ''
        };
        $scope.submit = function (valid) {
            if (!valid) {
                $.dialog.tips('注册信息有误', 1.5, 'error.gif');
                return;
            }
            $.post($.urlBuild('user', 'register'), $scope.data, function (data) {
                if (data.status == 1) {
                    $.dialog.alert('注册成功', function () {
                        $scope.$apply(function () {
                            $state.go('login');
                        });
                    });
                } else {
                    $.dialog.tips(data.info, 1.5, 'error.gif');
                }
            });
        };
    }]);
    app.controller('UserLoginController', ['$scope', '$state', 'UserService', function ($scope, $state, userService) {
        $scope.data = {
            username: '',
            password: ''
        };
        $scope.submit = function (valid) {
            if (!valid) {
                $.dialog.tips('登录信息有误', 1.5, 'error.gif');
                return;
            }
            $.post($.urlBuild('user', 'login'), $scope.data, function (data) {
                if (data.status == 1) {
                    userService.user.logined = true;
                    userService.user.username = $scope.data.username;
                    window.location.href = TPX.PATH_ROOT + '/';
                } else {
                    $.dialog.tips(data.info, 1.5, 'error.gif');
                }
            });
        };
    }]);
    app.controller('UserProfileController', ['$scope', '$state', '$http', 'UserService', function ($scope, $state, $http, userService) {
        $scope.data = {
            username: '',
            upload_space_space: '0 MB',
            upload_space_used: '0 MB'
        };
        $scope.submit = function (valid) {

        };

        loading.on();
        $.post($.urlBuild('user', 'load_profile'), {}, function (data) {
            loading.off();
            if (data.status == 1) {
                $scope.data.username = data.data.username;
                $scope.data.upload_space_space = data.data.upload_space_space;
                $scope.data.upload_space_used = data.data.upload_space_used;
            } else {
                $.defaultFormCallback(data);
            }
            $scope.$apply();
        });

    }]);
    app.controller('UserChangepwdController', ['$scope', '$state', '$http', 'UserService', function ($scope, $state, $http, userService) {
        $scope.data = {
            'password': '',
            'password-new': '',
            'password-repeat': ''
        };
        $scope.submit = function (valid) {
            if (!valid) {
                $.dialog.tips('数据信息有误', 1.5, 'error.gif');
                return;
            }
            $.post($.urlBuild('user', 'changepwd'), $scope.data, function (data) {
                if (data.status == 1) {
                    $scope.$apply(function () {
                        $.dialog.tips('修改成功', 1.5, 'success.gif');
                        $http.get($.urlBuild('user', 'logout'), {cache: false}).success(function (data) {
                            userService.user.logined = false;
                            userService.user.username = '';
                            $state.go('login');
                        });
                    });
                } else {
                    $.dialog.tips(data.info, 1.5, 'error.gif');
                }
            });
        };
    }]);


    app.controller('DocIndexController', ['$scope', '$state', 'UserService', function ($scope, $state, userService) {

    }]);


    // 文档分类的所有操作
    app.controller('DocCatController', ['$scope', '$state', '$http', 'UserService', 'DocCatService', 'DocService', function ($scope, $state, $http, userService, docCatService, docService) {

        $scope.docCatService = docCatService;
        $scope.docService = docService;

        docService.catScope = $scope;

        $scope.$watch('$viewContentLoaded', function () {
            $('#b-doc .c-cat').css({
                'height': $(window).height() - 40 + 'px'
            });
        });

        $scope.data = {
            id: 0,
            title: ''
        };


        $scope.edit = function (id, $event) {
            if ($event) {
                $event.stopPropagation();
            }
            id = id || 0;
            var cat = docCatService.load(id);
            if (null === cat) {
                $scope.data.title = '';
                $scope.data.id = 0;
            } else {
                $scope.data.title = cat.title;
                $scope.data.id = cat.id;
            }
            $('#cat-edit').modal();
        };

        $scope.delete = function () {
            docCatService.delete($scope.data, function (data) {
                if (data.status == 1 && data.info == 'ok') {
                    docCatService.reload(function () {
                        $scope.$apply();
                    });
                    $('#cat-edit').modal('hide');
                } else {
                    $.defaultFormCallback(data);
                }
            });
        };

        $scope.submit = function () {
            if ($scope.data.id > 0) {
                docCatService.edit($scope.data, function (data) {
                    if (data.status == 1 && data.info == 'ok') {
                        docCatService.reload(function () {
                            $scope.$apply();
                        });
                        $('#cat-edit').modal('hide');
                    } else {
                        $.defaultFormCallback(data);
                    }
                });
            } else {
                docCatService.add($scope.data, function (data) {
                    if (data.status == 1 && data.info == 'ok') {
                        docCatService.reload(function () {
                            $scope.$apply();
                        });
                        $('#cat-edit').modal('hide');
                    } else {
                        $.defaultFormCallback(data);
                    }
                });
            }
        };

        $scope.load = function (id) {
            docService.fetch(id, 1);
        };

    }]);

    app.controller('DocListController', ['$scope', '$state', 'DocService', function ($scope, $state, docService) {

        $scope.docService = docService;

        docService.listScope = $scope;

        $scope.$watch('$viewContentLoaded', function () {
            var $list = $('#b-doc .c-list .list');
            $list.css({
                'height': $(window).height() - 40 - 42 + 'px'
            });
            $list.scroll(function () {
                if ($('.list-loading', $list).isOnScreen()) {
                    docService.fetch(docService.fetch_cat_id);
                }
            });

            var load = function () {
                if (!docService.fetch_end && $('.list-loading', $list).isOnScreen()) {
                    docService.fetch(docService.fetch_cat_id);
                    setTimeout(function () {
                        load();
                    }, 50);
                }
            };
            load();

        });

        $.fn.isOnScreen = function () {
            var win = $(window);
            var viewport = {
                top: win.scrollTop(),
                left: win.scrollLeft()
            };
            viewport.right = viewport.left + win.width();
            viewport.bottom = viewport.top + win.height();
            var bounds = this.offset();
            bounds.right = bounds.left + this.outerWidth();
            bounds.bottom = bounds.top + this.outerHeight();
            return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
        };

        $scope.edit = function (id) {
            docService.load(id, function () {
                $scope.$apply();
            });
        };

    }]);

    app.controller('DocBodyController', ['$scope', '$state', 'UserService', 'DocCatService', 'DocService', function ($scope, $state, userService, docCatService, docService) {
        $scope.docCatService = docCatService;
        $scope.docService = docService;
        docService.bodyScope = $scope;
        $scope.$watch('$viewContentLoaded', function () {
            $('#iframe_editor,.mdeditor-box').css({
                'height': $(window).height() - 40 - 41 + 'px'
            });
        });
        $scope.fullscreen = function () {
            var o = $('#iframe_editor');
            if (o.attr('data-fullscreen') == 'true') {
                o
                    .attr('data-fullscreen', 'false')
                    .css({
                        position: '',
                        left: 0,
                        top: 0,
                        width: '100%',
                        height: $(window).height() - 40 - 41 + 'px'
                    });
                $('.btn-fullscreen img').attr('src', TPX.PATH_ASSERTS + '/res/img/icon_fullscreen.png');
            } else {
                o
                    .attr('data-fullscreen', 'true')
                    .css({
                        position: 'absolute',
                        left: 0,
                        top: 0,
                        width: $(window).width() + 'px',
                        height: $(window).height() + 'px'
                    });
                $('.btn-fullscreen img').attr('src', TPX.PATH_ASSERTS + '/res/img/icon_fullscreen_exit.png');
            }
            docService.fullscreenMarkdown();
        };

        $scope.submit = function () {
            docService.submit(function () {
                docCatService.reload(function () {
                    if (docService.catScope) {
                        docService.catScope.$apply();
                    }
                });
            });
        };

        $scope.delete = function () {
            docService.delete(function () {
                docCatService.reload(function () {
                    if (docService.catScope) {
                        docService.catScope.$apply();
                    }
                });
            });
        };

        $scope.history = function () {
            docService.fetch_history();
        };

        $scope.historyRestore = function (history_id) {
            docService.restore_history(history_id);
        };

        $scope.historyShow = function ($event) {
            var o = $($event.target);
            console.log(o.css('max-height'));
            if (o.css('max-height') == '150px') {
                o.css({
                    maxHeight: '100%'
                });
            } else {
                o.css({
                    maxHeight: '150px'
                });
            }
        };

    }]);


})
;