'use strict';
define('app', ['angular', 'angular-route', 'angular-ui-router', 'jquery.extern'], function (angular) {
    var app = angular.module('app', ['ngRoute', 'ui.router']);

    app.factory('UserLoginCheckInterceptor', ["$q", "$rootScope", '$location', 'UserService', function ($q, $rootScope, $location, userService) {
        return {
            response: function (response) {
                if (('data' in response) && (typeof response.data == 'object')) {
                    if ('status' in response.data) {
                        if (response.data.status == 0) {
                            if (response.data.info == 'login_required') {
                                $location.path('/login');
                            } else if (response.data.info == 'login_success') {
                                userService.user.logined = true;
                                userService.user.username = response.data.username;
                                $location.path('/doc');
                            }
                        }
                    }
                }
                return response;
            }
        };
    }]);

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.post["Content-Type"] =
            "application/x-www-form-urlencoded";
        $httpProvider.defaults.
            transformRequest.unshift(function (data,
                                               headersGetter) {
                var key, result = [];
                for (key in data) {
                    if (data.hasOwnProperty(key)) {
                        result.push(encodeURIComponent(key) + "="
                        + encodeURIComponent(data[key]));
                    }
                }
                return result.join("&");
            });
    }]);

    app.directive('ngFocus', [function () {
        var FOCUS_CLASS = "ng-focused";
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl) {
                ctrl.$focused = false;
                element.bind('focus', function (evt) {
                    element.addClass(FOCUS_CLASS);
                    scope.$apply(function () {
                        ctrl.$focused = true;
                    });
                }).bind('blur', function (evt) {
                    element.removeClass(FOCUS_CLASS);
                    scope.$apply(function () {
                        ctrl.$focused = false;
                    });
                });
            }
        }
    }]);

    app.directive('ngEqual', [function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                var target = '#' + attrs.ngEqual;
                elem.add(target).on('keyup', function () {
                    scope.$apply(function () {
                        var v = elem.val() === $(target).val();
                        ctrl.$setValidity('equal', v);
                    });
                });
            }
        }
    }]);

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('UserLoginCheckInterceptor');
    }]);


    return app;
});