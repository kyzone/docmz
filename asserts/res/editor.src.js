'use strict';
require([
        'jquery',
        "editormd",
        "../mdeditor/plugins/link-dialog/link-dialog",
        "../mdeditor/plugins/reference-link-dialog/reference-link-dialog",
        "../mdeditor/plugins/image-dialog/image-dialog",
        "../mdeditor/plugins/code-block-dialog/code-block-dialog",
        "../mdeditor/plugins/table-dialog/table-dialog",
        "../mdeditor/plugins/emoji-dialog/emoji-dialog",
        "../mdeditor/plugins/goto-line-dialog/goto-line-dialog",
        "../mdeditor/plugins/help-dialog/help-dialog",
        "../mdeditor/plugins/html-entities-dialog/html-entities-dialog",
        "../mdeditor/plugins/preformatted-text-dialog/preformatted-text-dialog"
    ], function ($, editormd) {

        $(function () {

            editormd.loadCSS(TPX.PATH_ASSERTS + "/mdeditor/lib/codemirror/addon/fold/foldgutter");

            window.parent.markdownEditor = editormd("markdown_editor", {
                width: "100%",
                path: TPX.PATH_ASSERTS + '/mdeditor/lib/',
                //theme: "dark",
                tex: true,
                emoji: true,
                taskList: true,
                flowChart: true,
                sequenceDiagram: true,
                onload: function () {
                    this.fullscreen();
                    this.addKeyMap({
                        "Ctrl-S": function (cm) {
                            window.parent.angular.element(window.parent.$('#command-submit')).scope().submit();
                        }
                    });
                },
                toolbarIcons: function () {
                    return [
                        //"undo", "redo", "|",
                        "bold", 'del', 'italic', 'quote', 'uppercase', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'list-ul', 'list-ol', "hr",
                        'link',
                        //'reference-link',
                        'image', 'code',
                        //'preformatted-text',
                        'code-block', 'table', 'datetime',
                        //'emoji',
                        'html-entities',
                        //'pagebreak',
                        'goto-line', 'watch',
                        //'unwatch',
                        'preview', 'search',
                        //'fullscreen',
                        //'clear',
                        'help'
                    ];
                },
                imageUpload: true,
                imageFormats: ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
                imageUploadURL: TPX.PATH_ROOT + '/?s=doc/upload_image'
            });

        });

    }
);

