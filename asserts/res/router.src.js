'use strict';
define('router', ['app'], function (app) {
    app.config(['$routeProvider', '$stateProvider', '$urlRouterProvider', function ($routeProvider, $stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: $.urlBuild('user', 'login')
            })
            .state('register', {
                url: '/register',
                templateUrl: $.urlBuild('user', 'register')
            })

            .state('user', {
                url: '/user',
                templateUrl: $.urlBuild('user', 'index'),
                controller: 'UserIndexController'
            })
            .state("user.profile", {
                url: "/profile",
                templateUrl: $.urlBuild('user', 'profile')
            })
            .state("user.changepwd", {
                url: "/changepwd",
                templateUrl: $.urlBuild('user', 'changepwd')
            })

            .state('doc', {
                url: '/doc',
                templateUrl: $.urlBuild('doc', 'index'),
                controller: 'DocIndexController'
            });


        $urlRouterProvider.otherwise('/doc');

    }]);
});