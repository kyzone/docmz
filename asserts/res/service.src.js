'use strict';
define('service', ['app'], function (app) {

    var loading = {
        cnt: 0,
        on: function () {
            var o = $('#status-loading');
            loading.cnt++;
            o.css({
                left: ($(window).width() - $(o).width()) / 2 + 'px'
            }).css({
                visibility: 'visible'
            });
        },
        off: function () {
            loading.cnt--;
            if (loading.cnt <= 0) {
                $('#status-loading').css({
                    visibility: 'hidden'
                });
            }
        },
        success_timeout: null,
        success: function (text, timeout) {
            var o = $('#status-success');
            timeout = timeout || 1500;
            o.html(text).css({
                left: ($(window).width() - $(o).width()) / 2 + 'px'
            }).css({
                visibility: 'visible'
            });
            if (loading.success_timeout) {
                clearTimeout(loading.success_timeout);
            }
            loading.success_timeout = setTimeout(function () {
                o.css({
                    visibility: 'hidden'
                });
            }, timeout);
        },
        error_timeout: null,
        error: function (text, timeout) {
            var o = $('#status-error');
            timeout = timeout || 1500;
            o.html(text).css({
                left: ($(window).width() - $(o).width()) / 2 + 'px'
            }).css({
                visibility: 'visible'
            });
            if (loading.error_timeout) {
                clearTimeout(loading.error_timeout);
            }
            loading.error_timeout = setTimeout(function () {
                o.css({
                    visibility: 'hidden'
                });
            }, timeout);
        }
    };


    app.service('UserService', [function () {
        var service = {
            user: User
        };
        return service;
    }]);


    app.service('DocService', [function () {
        var service = {
            listScope: null,
            bodyScope: null,
            url: '',
            data: [],
            one: {
                id: 0,
                out_id: '',
                cat_id: 0,
                title: '',
                content: ''
            },
            // 删除
            delete: function (callback) {
                if (!confirm('删除之后将不可恢复，确认？')) {
                    return;
                }
                callback = callback || null;
                if (!service.one.id) {
                    loading.error('文档为空');
                    return;
                }
                loading.on();
                $.post($.urlBuild('doc', 'delete'), service.one, function (data) {
                    loading.off();
                    if (data.status == 1) {
                        service.one.id = 0;
                        service.one.out_id = '';
                        var cat_id = service.one.cat_id;
                        service.one.cat_id = 0;
                        service.one.title = '';
                        service.one.content = '';
                        loading.success('删除成功');
                        if (service.bodyScope) {
                            service.bodyScope.$apply();
                        }
                        service.fetch(cat_id, 1);
                        if (callback) {
                            callback();
                        }
                    } else {
                        $.defaultFormCallback(data);
                    }
                });
            },
            // 提交
            submit: function (callback) {
                callback = callback || null;
                service.one.content = service.getMarkdown();
                if (!service.one.cat_id) {
                    loading.error('文档分类未选择');
                    return;
                }
                if (!service.one.title) {
                    loading.error('文档标题为空');
                    return;
                }
                if (!service.one.content) {
                    loading.error('文档内容为空');
                    return;
                }
                var url = $.urlBuild('doc', 'add');
                if (service.one.id) {
                    url = $.urlBuild('doc', 'edit');
                }

                loading.on();
                $.post(url, service.one, function (data) {
                    loading.off();
                    if (data.status == 1) {
                        service.one.id = data.data.id;
                        service.one.out_id = data.data.out_id;
                        service.one.update_time = data.data.update_time;
                        loading.success('保存成功');
                        if (service.bodyScope) {
                            service.bodyScope.$apply();
                        }
                        service.fetch(service.one.cat_id, 1);
                        if (callback) {
                            callback();
                        }
                    } else {
                        $.defaultFormCallback(data);
                    }
                });
            },
            // 加载
            load: function (id, callback) {
                if (id == 0) {
                    service.one.id = 0;
                    service.one.cat_id = 0;
                    service.one.title = '';
                    service.one.out_id = '';
                    service.one.content = '';
                    service.setMarkdown('');
                    return;
                }
                loading.on();
                $.post($.urlBuild('doc', 'load'), {id: id}, function (data) {
                    loading.off();
                    if (data.status == 1) {
                        service.one = data.data;
                        service.setMarkdown(service.one.content);
                        callback();
                    } else {
                        $.defaultFormCallback(data);
                    }
                });

            },
            // 获取列表
            fetch_page: 1,
            fetch_end: false,
            fetch_cat_id: 0,
            fetch_loading: false,
            fetch: function (cat_id, page) {
                if (service.fetch_loading) {
                    return;
                }
                if (typeof page == 'undefined') {
                    service.fetch_page++;
                    page = service.fetch_page;
                } else {
                    service.fetch_page = 1;
                    service.fetch_end = false;
                    service.fetch_cat_id = cat_id;
                    $('.list-loading').show();
                }
                service.fetch_loading = true;
                if (service.fetch_end) {
                    service.fetch_loading = false;
                    return;
                }
                loading.on();
                $.post($.urlBuild('doc', 'fetchall'), {cat_id: service.fetch_cat_id, page: page}, function (data) {
                    loading.off();
                    if (data.status == 1) {
                        service.cat_id = cat_id;
                        if (data.list.length == 0) {
                            if (page == 1) {
                                service.data = [];
                            }
                            service.fetch_end = true;
                            $('.list-loading').hide();
                        } else {
                            if (page == 1) {
                                service.data = [];
                            }
                            for (var i = 0; i < data.list.length; i++) {
                                service.data.push(data.list[i]);
                            }
                        }
                        if (service.listScope) {
                            service.listScope.$apply();
                        }
                    } else {
                        $.defaultFormCallback(data);
                    }
                    service.fetch_loading = false;
                });
            },
            setMarkdown: function (m) {
                if (!markdownEditor) {
                    return;
                }
                markdownEditor.setMarkdown(m);
            },
            getMarkdown: function () {
                if (!markdownEditor) {
                    return '';
                }
                return markdownEditor.getMarkdown();
            },
            fullscreenMarkdown: function () {
                if (!markdownEditor) {
                    return;
                }
                markdownEditor.fullscreen().fullscreen();
            },
            history_data: [],
            fetch_history: function () {
                loading.on();
                $.post($.urlBuild('doc', 'fetch_history'), {id: service.one.id}, function (data) {
                    loading.off();
                    if (data.status == 1) {
                        service.history_data = data.list;
                        if (service.bodyScope) {
                            service.bodyScope.$apply();
                        }
                        // 显示弹出框
                        $('#body-history .modal-dialog').css({
                            width: ($(window).width() > 800 ? 800 : $(window).width()) + 'px'
                        });
                        $('#body-history').modal();

                    } else {
                        $.defaultFormCallback(data);
                    }
                });
            },
            restore_history: function (id) {
                for (var i = 0; i < service.history_data.length; i++) {
                    if (service.history_data[i].id == id) {
                        var d = service.history_data[i];
                        service.setMarkdown(d.content);
                        $('#body-history').modal('hide');
                        loading.success('成功恢复 ' + d.update_time + ' 的历史记录到编辑器');
                        return;
                    }
                }
                loading.error('没有找到ID为 ' + id + ' 的历史记录');
            }

        };
        service.fetch(0, 1);
        return service;
    }]);


    app.service('DocCatService', ['$http', function ($http) {
        var service = {
            data: [],
            // 加载
            load: function (id) {
                for (var i = 0; i < service.data.length; i++) {
                    if (service.data[i].id == id) {
                        return service.data[i];
                    }
                }
                return null;
            },
            // 添加
            add: function (cat, callback) {
                loading.on();
                $.post($.urlBuild('doc_cat', 'add'), cat, function (data) {
                    loading.off();
                    callback(data);
                });
            },
            // 更新
            edit: function (cat, callback) {
                loading.on();
                $.post($.urlBuild('doc_cat', 'edit'), cat, function (data) {
                    loading.off();
                    callback(data);
                });
            },
            // 删除
            delete: function (cat, callback) {
                loading.on();
                $.post($.urlBuild('doc_cat', 'delete'), cat, function (data) {
                    loading.off();
                    callback(data);
                });
            },
            reload: function (callback) {
                callback = callback || null;
                loading.on();
                $.post($.urlBuild('doc_cat', 'fetchall'), {}, function (data) {
                    loading.off();
                    if (data.status == 1) {
                        service.data = data.list;
                    } else {
                        $.defaultFormCallback(data);
                    }
                    if (callback) {
                        callback();
                    }
                });
            }
        };
        service.reload();
        return service;
    }]);

});